package com.apolloagriculture.android.takehomeassignment.weather.usecases.viewweather

import com.apolloagriculture.android.takehomeassignment.weather.data.FetchWeather
import com.apolloagriculture.android.takehomeassignment.weather.data.WeatherJson
import com.apolloagriculture.android.takehomeassignment.weather.usecases.viewweather.ViewWeatherUseCase.Weather

class ViewWeatherUseCaseUsingApi(
    private val fetchWeather: FetchWeather,
) : ViewWeatherUseCase {

    override suspend fun invoke(): Result<Weather> =
        fetchWeather()
            .mapCatching { weatherJson ->
                Weather(
                    today = createDayWeather(weatherJson.today),
                    tomorrow = createDayWeather(weatherJson.tomorrow),
                    dayAfterTomorrow = createDayWeather(weatherJson.dayAfterTomorrow),
                )
            }

}

private fun createDayWeather(dayJson: WeatherJson.DayWeather): Weather.DayWeather =
    Weather.DayWeather(
        icon = when (dayJson.icon) {
            "CLEAR_DAY" -> Weather.DayWeather.Icon.CLEAR
            "SCATTERED_CLOUDS_DAY" -> Weather.DayWeather.Icon.SCATTERED_CLOUDS
            "BROKEN_OVERCAST_CLOUDS_DAY" -> Weather.DayWeather.Icon.BROKEN_OVERCAST_CLOUDS
            else -> throw IllegalArgumentException()
        },
        lowTemp = dayJson.lowTemp,
        highTemp = dayJson.highTemp,
        description = dayJson.description,
    )

