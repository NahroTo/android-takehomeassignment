package com.apolloagriculture.android.takehomeassignment.weather.data

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*

private const val API_URL = "https://s3.eu-west-1.amazonaws.com/assets.apolloagriculture.com/recruitment/android/weather.json"

class FetchWeather(
    private val httpClient: HttpClient,
) {

    suspend operator fun invoke(): Result<WeatherJson> = runCatching {
        httpClient.get(API_URL).body()
    }
}
