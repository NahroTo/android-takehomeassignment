package com.apolloagriculture.android.takehomeassignment.weather.di

import com.apolloagriculture.android.takehomeassignment.weather.data.FetchWeather
import com.apolloagriculture.android.takehomeassignment.weather.presentation.viewmodels.WeatherViewModelUsingUseCase
import com.apolloagriculture.android.takehomeassignment.weather.usecases.viewweather.ViewWeatherUseCase
import com.apolloagriculture.android.takehomeassignment.weather.usecases.viewweather.ViewWeatherUseCaseUsingApi
import io.ktor.client.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weatherModule = module {
    single {
        HttpClient {
            install(ContentNegotiation) {
                json(Json {
                    ignoreUnknownKeys = true
                })
            }
        }
    }
    single {
        FetchWeather(get())
    }
    single<ViewWeatherUseCase> {
        ViewWeatherUseCaseUsingApi(get())
    }
    viewModel {
        WeatherViewModelUsingUseCase(get())
    }
}
