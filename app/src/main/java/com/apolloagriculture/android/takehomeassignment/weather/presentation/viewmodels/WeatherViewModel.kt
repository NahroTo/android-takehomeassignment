package com.apolloagriculture.android.takehomeassignment.weather.presentation.viewmodels

import com.apolloagriculture.android.takehomeassignment.common.State
import kotlinx.coroutines.flow.StateFlow

interface WeatherViewModel {

    val state: StateFlow<State<WeatherScreenState>>
}
