package com.apolloagriculture.android.takehomeassignment.weather.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.apolloagriculture.android.takehomeassignment.R
import com.apolloagriculture.android.takehomeassignment.common.State
import com.apolloagriculture.android.takehomeassignment.common.presentation.views.ComposeScreenFragment
import com.apolloagriculture.android.takehomeassignment.common.presentation.views.handleScreenState
import com.apolloagriculture.android.takehomeassignment.weather.presentation.viewmodels.WeatherScreenState
import com.apolloagriculture.android.takehomeassignment.weather.presentation.viewmodels.WeatherViewModelUsingUseCase
import org.koin.androidx.compose.getViewModel

class WeatherFragment : ComposeScreenFragment() {

    @Composable
    override fun Content() {

        // Not using interface WeatherViewModel because Koin requires androidx.ViewModel.
        val viewModel = getViewModel<WeatherViewModelUsingUseCase>()

        val screenState: State<WeatherScreenState> by viewModel.state.collectAsState()

        handleScreenState(screenState) { state ->
            Column(modifier = Modifier.padding(16.dp)) {
                Text(
                    text = stringResource(R.string.weather_label),
                    style = MaterialTheme.typography.h2,
                    color = Color.Green,
                )
                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 32.dp),
                ) {
                    DayWeatherInfo(state.todayWeather)
                    DayWeatherInfo(state.tomorrowWeather)
                    DayWeatherInfo(state.dayAfterTomorrowWeather)
                }
            }
        }

    }

}

@Composable
private fun DayWeatherInfo(state: WeatherScreenState.DayWeatherInfoState) = with(state) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Image(
            painter = painterResource(
                id = when (icon) {
                    WeatherScreenState.DayWeatherInfoState.Icon.CLEAR -> R.drawable.ic_weather_cloud_sun
                    WeatherScreenState.DayWeatherInfoState.Icon.SCATTERED_CLOUDS -> R.drawable.ic_weather_some_clouds
                    WeatherScreenState.DayWeatherInfoState.Icon.BROKEN_OVERCAST_CLOUDS -> R.drawable.ic_weather_one_cloud
                },
            ),
            contentDescription = "Weather Icon",
            modifier = Modifier.size(width = 48.dp, height = 36.dp),
        )
        Text(
            text = "$lowTemp-$highTemp C",
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(top = 24.dp),
        )
        Text(
            text = description,
            textAlign = TextAlign.Center,
        )
    }
}

