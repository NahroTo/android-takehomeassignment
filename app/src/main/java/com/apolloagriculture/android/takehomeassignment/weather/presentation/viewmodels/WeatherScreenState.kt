package com.apolloagriculture.android.takehomeassignment.weather.presentation.viewmodels

data class WeatherScreenState(
    val todayWeather: DayWeatherInfoState,
    val tomorrowWeather: DayWeatherInfoState,
    val dayAfterTomorrowWeather: DayWeatherInfoState,
) {

    data class DayWeatherInfoState(
        val icon: Icon,
        val lowTemp: Int,
        val highTemp: Int,
        val description: String,
    ) {
        enum class Icon {
            CLEAR, SCATTERED_CLOUDS, BROKEN_OVERCAST_CLOUDS
        }
    }

}
