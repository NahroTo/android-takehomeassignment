package com.apolloagriculture.android.takehomeassignment.weather.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apolloagriculture.android.takehomeassignment.common.State
import com.apolloagriculture.android.takehomeassignment.common.asState
import com.apolloagriculture.android.takehomeassignment.common.mapDataToData
import com.apolloagriculture.android.takehomeassignment.weather.usecases.viewweather.ViewWeatherUseCase
import kotlinx.coroutines.flow.*
import kotlin.math.roundToInt

class WeatherViewModelUsingUseCase(
    private val viewWeatherUseCase: ViewWeatherUseCase,
) : ViewModel(), WeatherViewModel {

    private val weatherState: StateFlow<State<ViewWeatherUseCase.Weather>> = flow {
        emit(State.Loading)
        emit(viewWeatherUseCase().asState())
    }
        .stateIn(viewModelScope, SharingStarted.Eagerly, State.Loading)

    override val state: StateFlow<State<WeatherScreenState>> =
        weatherState
            .mapDataToData { weather ->
                WeatherScreenState(
                    todayWeather = createDayWeatherInfoState(weather.today),
                    tomorrowWeather = createDayWeatherInfoState(weather.tomorrow),
                    dayAfterTomorrowWeather = createDayWeatherInfoState(weather.dayAfterTomorrow),
                )

            }
            .stateIn(viewModelScope, SharingStarted.Eagerly, State.Loading)


}

private fun createDayWeatherInfoState(
    dayWeather: ViewWeatherUseCase.Weather.DayWeather,
): WeatherScreenState.DayWeatherInfoState =
    WeatherScreenState.DayWeatherInfoState(
        icon = when (dayWeather.icon) {
            ViewWeatherUseCase.Weather.DayWeather.Icon.CLEAR ->
                WeatherScreenState.DayWeatherInfoState.Icon.CLEAR

            ViewWeatherUseCase.Weather.DayWeather.Icon.SCATTERED_CLOUDS ->
                WeatherScreenState.DayWeatherInfoState.Icon.SCATTERED_CLOUDS

            ViewWeatherUseCase.Weather.DayWeather.Icon.BROKEN_OVERCAST_CLOUDS ->
                WeatherScreenState.DayWeatherInfoState.Icon.BROKEN_OVERCAST_CLOUDS
        },
        lowTemp = dayWeather.lowTemp.roundToInt(),
        highTemp = dayWeather.highTemp.roundToInt(),
        description = dayWeather.description,
    )

