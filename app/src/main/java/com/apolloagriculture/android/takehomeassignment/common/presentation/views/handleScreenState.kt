package com.apolloagriculture.android.takehomeassignment.common.presentation.views

import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.apolloagriculture.android.takehomeassignment.common.State

@Composable
inline fun <T> handleScreenState(state: State<T>, block: (T) -> Unit) {
    when (state) {
        is State.Loading -> {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier.fillMaxSize(),
            ) {
                CircularProgressIndicator()
            }
        }
        is State.Error -> {
            SideEffect {
                Log.e(null, "Error", state.throwable)
            }
        }
        is State.Data -> {
            block(state.data)
        }
    }
}