package com.apolloagriculture.android.takehomeassignment.common

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

sealed interface State<out T> {
    data class Error(val throwable: Throwable) : State<Nothing>

    object Loading : State<Nothing>

    data class Data<out T>(val data: T) : State<T>
}

fun <T> Result<T>.asState(): State<T> =
    if (isSuccess) {
        State.Data(getOrThrow())
    } else {
        State.Error(exceptionOrNull()!!)
    }

inline fun <T, R> Flow<State<T>>.mapDataToData(
    crossinline transform: suspend (data: T) -> R,
): Flow<State<R>> = mapDataToState { data -> State.Data(transform(data)) }

inline fun <T, R> Flow<State<T>>.mapDataToState(
    crossinline transform: suspend (data: T) -> State<R>,
): Flow<State<R>> =
    map { state ->
        state.mapDataToState { data ->
            transform(data)
        }
    }

inline fun <T, R> State<T>.mapDataToState(
    transform: (data: T) -> State<R>,
): State<R> =
    when (this) {
        is State.Loading -> this
        is State.Error -> this
        is State.Data -> transform(data)
    }
