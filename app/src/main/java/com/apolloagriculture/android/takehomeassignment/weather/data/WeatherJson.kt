package com.apolloagriculture.android.takehomeassignment.weather.data

import kotlinx.serialization.Serializable

@Serializable
data class WeatherJson(
    val today: DayWeather,
    val tomorrow: DayWeather,
    val dayAfterTomorrow: DayWeather,
) {
    @Serializable
    data class DayWeather(
        val lowTemp: Float,
        val highTemp: Float,
        val icon: String,
        val description: String,
    )
}
