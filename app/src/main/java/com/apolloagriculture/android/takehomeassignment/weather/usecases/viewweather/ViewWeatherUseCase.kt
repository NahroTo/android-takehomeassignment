package com.apolloagriculture.android.takehomeassignment.weather.usecases.viewweather

interface ViewWeatherUseCase {

    data class Weather(
        val today: DayWeather,
        val tomorrow: DayWeather,
        val dayAfterTomorrow: DayWeather,
    ) {

        data class DayWeather(
            val icon: Icon,
            val lowTemp: Float,
            val highTemp: Float,
            val description: String,
        ) {
            enum class Icon {
                CLEAR, SCATTERED_CLOUDS, BROKEN_OVERCAST_CLOUDS
            }
        }

    }
    suspend operator fun invoke(): Result<Weather>
}
